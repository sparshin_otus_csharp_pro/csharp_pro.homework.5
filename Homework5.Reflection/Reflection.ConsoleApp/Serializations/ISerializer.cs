﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reflection.ConsoleApp.Serializations
{
    public interface ISerializer<T>
    {
        public string Serialize(T item);
        public T Deserialize(string item);


    }
}
