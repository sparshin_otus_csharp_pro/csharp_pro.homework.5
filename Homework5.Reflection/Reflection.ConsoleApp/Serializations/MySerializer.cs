﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reflection.ConsoleApp.Serializations
{
    public class MySerializer<T>: ISerializer<T>
    {
        public string Serialize(T item)
        {
            var builder = new StringBuilder();

            var fields = typeof(T).GetFields();

            builder
                .Append("{{")
                .Append(string.Join($";", fields.Select(t => $"{t.Name}:{t.GetValue(item)}")))
                .Append("}}");

            return builder.ToString();
        }

        //десерилизовать умеет только поля и только базовых типов, увы. Но на тестовый пример в задаче натягивается)).
        public T Deserialize(string item)
        {
            if (string.IsNullOrEmpty(item))
            {
                throw new ArgumentNullException("Incoming string is null or empty!");
            }

            var parsedElements = item.Replace("{", string.Empty).Replace("}", string.Empty).Trim().Split(";");

            var fields = parsedElements.Select(t => t.Split(":")).Select(t => new KeyValuePair<string, string>(t[0], t[1]));

            var instance = (T)Activator.CreateInstance(typeof(T));

            foreach (var field in fields)
            {
                var objField = instance.GetType().GetField(field.Key);
                if (objField != null)
                {
                    var objFieldType = objField.FieldType;
                    var convertedValue = Convert.ChangeType(field.Value, objFieldType);

                    objField.SetValue(instance, convertedValue);
                }
            }

            return instance;
        }
    }
}
