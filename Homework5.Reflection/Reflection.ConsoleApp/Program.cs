﻿using Microsoft.Extensions.Configuration;
using Reflection.ConsoleApp.Models;
using Reflection.ConsoleApp.Serializations;
using Serilog;
using System.Diagnostics;
using System.Text.Json;

namespace Reflection.ConsoleApp
{
    internal class Program
    {
        static Serilog.Core.Logger _logger;
        
        static Stopwatch stopWatch = new Stopwatch();

        const int ITERATIONS_COUNT = 50000;

        static async Task Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            _logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .WriteTo.Console()
                .CreateLogger();
            
            _logger.Information($"*****************************************************************");
            _logger.Information($"My serializer:");
            _logger.Information($"Serialize:");

            string mySerializedF = "";
            var mySerializer = new MySerializer<F>();

            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < ITERATIONS_COUNT; i++)
            {
                mySerializedF = mySerializer.Serialize(F.Get());
            }
            stopWatch.Stop();

            _logger.Information($"Result string: {mySerializedF}");
            _logger.Information($"Serialization time total: {stopWatch.Elapsed.TotalMilliseconds}; Serialization time per unit: {stopWatch.Elapsed.TotalMilliseconds / ITERATIONS_COUNT}");

            _logger.Information($"Deserialize:");
            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < ITERATIONS_COUNT; i++)
            {
                var obj = mySerializer.Deserialize(mySerializedF);
            }
            stopWatch.Stop();
            _logger.Information($"Deserialization time total: {stopWatch.Elapsed.TotalMilliseconds}; Deserialization time per unit: {stopWatch.Elapsed.TotalMilliseconds / ITERATIONS_COUNT}");
            
            _logger.Information($"*****************************************************************");
            _logger.Information($"System.Json:");
            _logger.Information($"Serialize:");

            string jsonSerializedF = "";

            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < ITERATIONS_COUNT; i++)
            {
                jsonSerializedF = JsonSerializer.Serialize(F.Get());
            }
            stopWatch.Stop();

            _logger.Information($"Serialization time total: {stopWatch.Elapsed.TotalMilliseconds}; Serialization time per unit: {stopWatch.Elapsed.TotalMilliseconds / ITERATIONS_COUNT}");
           
            _logger.Information($"Deserialize:");

            stopWatch.Reset();
            stopWatch.Start();
            for (int i = 0; i < ITERATIONS_COUNT; i++)
            {
                JsonSerializer.Deserialize<F>(jsonSerializedF);
            }
            stopWatch.Stop();
            
            _logger.Information($"Deserialization time total: {stopWatch.Elapsed.TotalMilliseconds}; Deserialization time per unit: {stopWatch.Elapsed.TotalMilliseconds / ITERATIONS_COUNT}");
            _logger.Information($"*****************************************************************");


            _logger.Information("Good bye!");
            Console.ReadKey();
        }
    }
}